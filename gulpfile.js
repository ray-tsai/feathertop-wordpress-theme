/*================================
=            Requires            =
================================*/
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    nano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    browserify = require('browserify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    watch = require('gulp-watch'),
    changed = require('gulp-changed'),
    rimraf = require('gulp-rimraf'),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    browserSync = require('browser-sync').create();

/*=================================
=            Functions            =
=================================*/
function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

/*=============================
=            Tasks            =
=============================*/

// Browser sync
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://localhost/feathertop/"
    });
});


// Style vendors
gulp.task('vendors-styles', function() {
    return gulp.src('src/vendors/styles/*.css')
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest('dist/vendors/styles'))
    // add into sass
    .pipe(rename({ suffix: '', extname: '.scss' }))
    .pipe(gulp.dest('src/sass/vendors'))
});


// Sass
gulp.task('sass', function() {
    gulp.src('src/sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({outputStyle: 'nested'}))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('dist/styles'))
    .pipe(gulp.dest(''))
    .pipe(browserSync.stream())
    //.pipe(gulp.dest(''))
    .pipe(notify({ message: 'Sass compiled complete' }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(nano())
    .pipe(gulp.dest('dist/styles/'))
    .pipe(browserSync.stream())
    .pipe(notify({ message: 'Sass Minimized complete' }))
    .pipe(notify({ message: 'Sass Task complete' }));
});

// Scripts
gulp.task('scripts', function() {
  return browserify( './src/scripts/main.js' )
    .bundle()
    .on('error', handleError)
    .pipe(source('build.js'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(notify({ message: 'Scripts Main complete' }))
    .pipe(buffer())
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(notify({ message: 'Scripts Minimized complete' }))
    .pipe(notify({ message: 'Scripts Task complete' }));
});
 
// Assets
gulp.task('assets', function() {
  return gulp.src('src/asst/**/*')
    .pipe(changed('dist/asst'))
    .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/asst'))
    .pipe(notify({ message: 'Assets task complete' }));
});
 
// Clean
gulp.task('clean', function(cb) {
return gulp.src( [ 'dist/styles', 'dist/asst', 'dist/scripts' ], { read: false } ) // much faster
  .pipe(rimraf());
});


// Watch
gulp.task('watch', function() {

    // Watch Vendors styles
    watch('src/vendors/styles/*.css', function () {
        gulp.start( 'vendors-styles' );
    });

    // Watch Sass
    watch('src/sass/**/*.scss', function () {
        gulp.start( 'sass' );
    });

    // Watch Assets
    watch( 'src/asst/**/*' , function () {
        gulp.start( 'assets' );
    });

    // Watch Scripts
    watch( 'src/scripts/**/*.js' , function () {
        gulp.start( 'scripts' );
    });

});
 
// Default task
gulp.task('default', ['clean'], function(){
    gulp.start( 'vendors-styles', 'sass', 'scripts', 'assets', 'watch', 'browser-sync' );
});
